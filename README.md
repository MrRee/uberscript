# UberScript Reborn
UberScript was a script for XChat which have both been abandoned.  
Uberscript Reborn brings UberScript to HexChat with a few bug fixes and modifications to the default configuration.

## Brief list of functions:
### Core:
* Adds new Menu item controlling most aspects. 
* Adds several User Buttons
* Automatically saves configuration.
* Wide variety of macros for all output strings.

### Fun/Silly: 
* Random Slaps, including third person.
* Random Gives, including third person.
* Random Quotes, including multiple quote files. 
* Random Quit Messages.
* Random CTCP VERSION replies. 
* "Rainbow" text with /uc Text

### Useful: 
* Full Trigger support! Respond to user commands or your own commands, say/me/kick/op - whatever you want.
* Join/Part Filter. 
* Nick-change filter. 
* Now Playing / Junk Strings filter.
* Auto Greet, Auto-op, Auto-Voice (Any action on join, specified nick/channel or wildcard)
* Quakenet automatic Q authing, including hiding hostmask.
* Automatic reply to queries, toggleable for here and away. (Doesn't keep spamming same message each line, either)
* A Favourites list - Adds a menu containing your favourite channels for one-click joining. /addfav and /delfav
* A Quickstrings list - A menu system where you can add strings to a special menu. Eg, Auth strings you don't need to call every time, special op stuff that you can't remember - just bung it in the self-sorting menu and it'll be there, just two clicks away. /addquick and /delquick
* An easy menu system for all the HexChat internal toggles, along with descriptions.

# Installation
Just drop `UberScript.pl` along with the `uber/` & `quotes/` directories into your HexChat `addons/` directory. Then type `/load UberScript.pl` into HexChat and you're good to go!

# Support
If you need any help with UberScript reborn, want to report a bug, or just want a place to chill and hang out you can find me on `irc.6697.co.uk` in `#uberscript`.

# Macros supported
These work in most strings - Greetings, Triggers etc. The exceptions are things in the Quickstrings menu. 
Simply include them in your response/string and they'll be expanded when used.

* `%n` = Nick of third person.
* `%m` = Your current nick.
* `%s` = Current server. (In multi-server situations, will be the one in context)
* `%e` = Current network. 
* `%t` = Topic of current channel.
* `%c` = Current channel.
* `%r` = Your /away reason.
* `%u` = Random user from current channel.
* `%l` = Entire line of text.
* `%1` = First word in line
* `%2` = Second word in line
* `%3` = Third word in line
* `%4` = Fourth word in line
* `%5` = Fifth word in line
* `%6` = Sixth word in line
* `%i` = Current time - HH:MM.S
* `%d` = Current date - Day/Mon/Year
* `%w` = Current day of week. Monday, Tuesday etc.
* `%h` = Number of channels connected to.
* `%a` = Number of queries/PMs open.

# TODO
* Update checker
* Winamp now playing for Windows
* Other player options for now playing commands, swappable via `/uset` command.
* Updated readme with more useful information about what the script does + macros available.

# Changelog
## v1.24 - so far
* Bugfix for Rhythmbox now playing
* Regain nick now off in default configuration.

## v1.23
* Remove alpha-characters from version numbers.
* Added OS detection for OS specific commands.
* Rhythmbox now playing on Linux with the `/ubernp` command.

## v1.2b
* Fixed bug where `/uberslap` pulled from the gives file instead of the slaps file.
* Fixed timer bug with `/uberslap` command.

## v1.2a
* Ported code for HexChat.